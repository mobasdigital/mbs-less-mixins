# LESS Mixins Readme

This is a repository which can be used as a starting point for up to date mixins to use on new website builds.

As a rule we are not going to be providing mixins which merely add prefix support as we are moving towards using
autoprefixer where possible and the need for most prefixes is diminishing all the time. There may be exceptions
to this rule.